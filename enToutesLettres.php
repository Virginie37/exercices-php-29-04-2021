<?php
echo "Entrer un nombre entre 1 et 20 : \n";
$nombre = intval(readline());

switch ($nombre){
    case 1:
        echo "un\n";
        break;
    case 2:
        echo "deux\n";
        break;
    case 3:
        echo "trois\n";
        break;
    case 4:
        echo "quatre\n";
        break;
    case 5:
        echo "cinq\n";
        break;
    case 6:
        echo "six\n";
        break;
    case 7:
        echo "sept\n";
        break;
    case 8:
        echo "huit\n";
        break;
    case 9:
        echo "neuf\n";
        break;
    case 10:
        echo "dix\n";
        break;
    case 11:
        echo "onze\n";
        break;
    case 12:
        echo "douze\n";
        break;
    case 13:
        echo "treize\n";
        break;
    case 14:
        echo "quatorze\n";
        break;
    case 15:
        echo "quinze\n";
        break;
    case 16:
        echo "seize\n";
        break;
    case 17:
        echo "dix-sept\n";
        break;
    case 18:
        echo "dix-huit\n";
        break;
    case 19:
        echo "dix-neuf\n";
        break;
    case 20:
        echo "vingt\n";
        break;
    default:
        echo "nombre inconnu\n";
    }