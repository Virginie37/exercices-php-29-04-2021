<?php
echo "Entrer un nombre : \n";
$nb = intval(readline());

for ($i = 2; $i < $nb; $i++) {
    if ($nb % $i == 0) {
        $nbPremier = false;
        break;
    } else {
        $nbPremier = true;
    }
}
if ($nbPremier or $nb==1) {
    echo "$nb est un nombre premier.\n";
} else {
    echo "$nb n'est pas un nombre premier.\n";
}